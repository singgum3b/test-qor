package admin

import (
	"github.com/qor/validations"
	"github.com/qor/admin"
	"github.com/qor/qor"
	"github.com/qor/roles"
	// =======================
	"test-qor/models"
	"test-qor/config/auth"
	"test-qor/db"
	"test-qor/utils"
)

var (
	Admin *admin.Admin
)

func init()  {

	validations.RegisterCallbacks(db.Connection)

	db.Connection.AutoMigrate(&models.Room{}, &models.Interior{}, &models.User{})

	// Initalize
	Admin = admin.New(&qor.Config{DB: db.Connection})

	Admin.SetSiteName("Qor test")
	auth.HandleAuthentication(Admin)

	// Create resources from GORM-backend model
	Admin.AddResource(&models.Room{}, &admin.Config{ Permission: roles.Allow(roles.Read, roles.Anyone) })
	user := Admin.AddResource(&models.User{}, &admin.Config{ Permission: roles.Allow(roles.CRUD, roles.Anyone) })
	user.NewAttrs("Email", "Name", "Password")
	user.ShowAttrs("Email")
	user.Meta(&admin.Meta{Name: "Email", Type: "string"})

	Admin.AddResource(&models.Interior{})

	Admin.RegisterFuncMap("csrf_token", func(ctx *admin.Context) string {
		return utils.CSRF_Token(ctx.Request)
	})
}
