package auth

import (
	"github.com/qor/qor"
	"github.com/qor/admin"
	"test-qor/models"
)

type QORAdminAuth struct {}

func (QORAdminAuth) LoginURL(c *admin.Context) string {
	return "/auth/login"
}

func (QORAdminAuth) LogoutURL(c *admin.Context) string {
	return "/auth/logout"
}

func (QORAdminAuth) GetCurrentUser(c *admin.Context) qor.CurrentUser {
	user, err := AuthbossInstance.CurrentUser(c.Writer, c.Request)
	if err != nil {
		return nil
	}
	return user.(*models.User)
}
