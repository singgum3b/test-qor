package auth

import (
	"net/http"
	"io"
	"log"
	"os"
	"fmt"
	"html/template"
	// ==========================
	"gopkg.in/authboss.v1"
	"github.com/qor/admin"
	"github.com/justinas/nosurf"
	_ "gopkg.in/authboss.v1/auth"
	_ "gopkg.in/authboss.v1/register"
)

var (
	AuthbossInstance *authboss.Authboss
	_ = fmt.Println
)

func init()  {
	// Authentication
	AuthbossInstance = authboss.New()
	AuthbossInstance.MountPath = "/auth"
	AuthbossInstance.ViewsPath = "./views/"
	AuthbossInstance.LogWriter = os.Stdout
	AuthbossInstance.XSRFName = "csrf_token"
	AuthbossInstance.Layout = template.Must(template.New("application.html.tpl").Funcs(template.FuncMap{
		"t": func(i18Key string, args ...interface{}) string {
			if len(args) > 0 {
				if args[0] == nil {
					return i18Key
				} else {
					return fmt.Sprint(args[0])
				}
			}
			return ""
		},
	}).ParseFiles("./views/application.html.tpl"))

	AuthbossInstance.XSRFMaker = func(_ http.ResponseWriter, r *http.Request) string {
		return nosurf.Token(r)
	}
	AuthbossInstance.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		io.WriteString(w, "No x found")
	})
	AuthbossInstance.ErrorHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w, "Error x found")
	})

	AuthbossInstance.SessionStoreMaker = func(writer http.ResponseWriter, req *http.Request) authboss.ClientStorer {
		return SessionStorer{ w: writer, r: req }
	}

	AuthbossInstance.CookieStoreMaker = func(w http.ResponseWriter, r *http.Request) authboss.ClientStorer {
		return SessionStorer{ w, r }
	}

}

func HandleAuthentication(adm *admin.Admin) *admin.Admin {

	AuthbossInstance.Storer = Storer{ DB: adm.Config.DB }

	if err := AuthbossInstance.Init(); err != nil {
		// Handle error, don't let program continue to run
		log.Println("Fatal error when trying to initialize Authboss instance")
		log.Fatalln(err)
	}

	adm.SetAuth(&QORAdminAuth{})

	return adm
}

