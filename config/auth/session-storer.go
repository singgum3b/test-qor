package auth

import (
	"net/http"
	"github.com/alexedwards/scs/session"
)

type SessionStorer struct {
	w http.ResponseWriter
	r *http.Request
}


func (s SessionStorer) Put(key string, val string) {
	session.PutString(s.r, key, val)
}

func (s SessionStorer) Get(key string) (string, bool) {
	val,err := session.GetString(s.r, key)
	if (err != nil) {
		return "", false
	}
	return val, true
}

func (s SessionStorer) Del(key string) {
	session.Remove(s.r, key)
}
