package auth

import (
	"test-qor/models"
	"github.com/jinzhu/gorm"
	"gopkg.in/authboss.v1"
	"fmt"
)

// Implement authboss Storer Interface
type Storer struct {
	DB *gorm.DB
}

func (s Storer) Create(key string, attr authboss.Attributes) error {
	var user models.User
	if err := attr.Bind(&user, true); err != nil {
		return err
	}

	if err := s.DB.Create(user).Error; err != nil {
		return err
	}

	return nil
}

func (s Storer) Get(key string) (interface{}, error) {

	if key == "" {
		return nil, authboss.ErrUserNotFound
	}

	var user models.User
	if err := s.DB.Where("email = ?", key).First(&user).Error; err != nil {
		fmt.Println(err)
		return nil, authboss.ErrUserNotFound
	}

	return &user, nil
}

func (s Storer) Put(key string, attr authboss.Attributes) error {

	return nil
}
