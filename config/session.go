package config

import (
	"github.com/alexedwards/scs/session"
	"github.com/alexedwards/scs/engine/cookiestore"
	"net/http"
	"log"
)

// HMAC authentication key (hexadecimal representation of 32 random bytes)
var hmacKey = []byte("f71dc7e58abab014ddad2652475056f185164d262869c8931b239de52711ba87")
// AES encryption key (hexadecimal representation of 16 random bytes)
var blockKey = []byte("911182cec2f206986c8c82440adb7d17")

func HandleSession(mux *http.ServeMux) http.Handler {
	keyset, err := cookiestore.NewKeyset(hmacKey, blockKey)
	if err != nil {
		log.Fatal(err)
	}
	engine := cookiestore.New(keyset)
	manager := session.Manage(engine)

	return manager(mux)
}
