package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
	"log"
)

var (
	Connection *gorm.DB
)

func init()  {
	var errDB error
	Connection, errDB = gorm.Open("sqlite3", "./../../demo.db")

	if errDB != nil {
		log.Println("Error when trying to open DB connection")
		log.Fatalln(errDB)
	}
}
