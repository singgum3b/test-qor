package main

import (
	"fmt"
	"net/http"
	// =========================
	"test-qor/config"
	"test-qor/config/admin"
	"test-qor/config/auth"
	"github.com/justinas/nosurf"
)

func main() {

	// Register route
	mux := http.NewServeMux()
	mux.Handle("/auth/", auth.AuthbossInstance.NewRouter())
	// amount to /admin, so visit `/admin` to view the admin interface
	admin.Admin.MountTo("/admin", mux)

	fmt.Println("Listening on: 9000")
	http.ListenAndServe(":9000", nosurf.New(config.HandleSession(mux)))
}
