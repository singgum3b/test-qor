package models

import (
	"github.com/jinzhu/gorm"
)

type Interior struct {
	gorm.Model
	Code string `gorm:"unique;" valid:"required"`
	Name string `valid:"required"`
}
