package models

import "github.com/jinzhu/gorm"

type Room struct {
	gorm.Model
	Code string `gorm:"unique;" valid:"required"`
	Name string `valid:"required"`
	InteriorList []Interior `gorm:"many2many:room_interior;"`
}
