package models

type User struct {
	Email string `gorm:"primary_key"`
	Password string `valid:"required"`
	Name string
}

func (user User) DisplayName() string {
	return user.Email
}
