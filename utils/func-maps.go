package utils

import (
	"net/http"
	"github.com/justinas/nosurf"
)

func CSRF_Token(r *http.Request) string {
	return nosurf.Token(r);
}
