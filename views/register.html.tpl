<div class="container">
    <div class="row">
        <div class="column column-12">
            <div class="auth register">
                <form action="{{mountpathed "register"}}" method="post">
                    <ul class="auth-form">
                        <li>
                            <label for="{{.primaryID}}">{{ t "register.email" "Email" }}</label>
                            <input name="{{.primaryID}}" type="text" value="{{with .primaryIDValue}}{{.}}{{end}}" placeholder="{{title .primaryID}}" />
                            {{$pid := .primaryID}}{{with .errs}}{{with $errlist := index . $pid}}{{range $errlist}}<p class="error">{{.}}</p>{{end}}{{end}}{{end}}
                        </li>



                        <li>
                            <label for="password">{{ t "register.password" "Password" }}</label>
                            <input name="password" type="password" placeholder="{{ t "register.password" "Password" }}" />
                            {{with .errs}}{{range .password}}<p class="error">{{.}}</p>{{end}}{{end}}
                        </li>

                        <li>
                            <label for="confirm_password">{{ t "register.confirm_password" "Confirm Password" }}:</label>
                            <input name="confirm_password" type="password" placeholder="{{ t "register.confirm_password" "Confirm Password" }}" />
                            {{with .errs}}{{range .confirm_password}}<p class="error">{{.}}</p>{{end}}{{end}}
                        </li>

                        <li><button type="submit" class="button button__primary">{{ t "register.button" "Register" }}</button></li>

                        <input type="hidden" name="{{.xsrfName}}" value="{{.xsrfToken}}" />
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
